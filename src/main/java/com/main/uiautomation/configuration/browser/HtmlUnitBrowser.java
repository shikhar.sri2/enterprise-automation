/**
 * 
 */
package com.main.uiautomation.configuration.browser;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class HtmlUnitBrowser {

	
	public WebDriver getHtmlUnitDriver() {
		return new HtmlUnitDriver();
	}
}
