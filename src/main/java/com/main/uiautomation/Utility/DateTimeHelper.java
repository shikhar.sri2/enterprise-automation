/**
 * 
 */
package com.main.uiautomation.Utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.IsoFields;
import java.util.Calendar;

import javax.swing.text.DateFormatter;

import static java.time.temporal.TemporalAdjusters.*;

public class DateTimeHelper {

	public static String getCurrentDateTime() {

		DateFormat dateFormat = new SimpleDateFormat("_yyyy-MM-dd_HH-mm-ss");
		Calendar cal = Calendar.getInstance();
		String time = "" + dateFormat.format(cal.getTime());
		return time;
	}

	public static String getCurrentDate() {
		return getCurrentDateTime().substring(0, 11);
	}

	/**
	 * This method returns current Local Date
	 * 
	 * @return
	 */
	public String getCurrentLocalDate() {
		LocalDate today = LocalDate.now();
		String todaysDateInString = today.toString();
		return todaysDateInString;
	}
	
	/**
	 * This method returns current Local Date
	 * 
	 * @return
	 */
	public String getCurrentLocalDateTime() {
		LocalDateTime today = LocalDateTime.now();
		String todaysDateInString = today.toString();
		return todaysDateInString;
	}

}
