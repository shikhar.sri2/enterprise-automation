package com.main.uiautomation.PageObject;

import com.main.uiautomation.helper.Action.ActionHelper;
import com.main.uiautomation.helper.Dropdown.DropDownHelper;
import com.main.uiautomation.helper.TestBase.TestBase;
import com.main.uiautomation.helper.Wait.WaitHelper;
import com.main.uiautomation.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.logging.Logger;

public class ForgotPasswordPage extends TestBase {

	public WebDriver driver;
	Logger logger;
	private GenericHelper objGenericHelper;
	private WaitHelper objWaitHelper;
	private DropDownHelper objDropDownHelper;
	private ActionHelper objActionHelper;

	// String browserName;
	public ForgotPasswordPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		objGenericHelper = PageFactory.initElements(driver, GenericHelper.class);
		objWaitHelper = PageFactory.initElements(driver, WaitHelper.class);
		objDropDownHelper = PageFactory.initElements(driver, DropDownHelper.class);
		objActionHelper = PageFactory.initElements(driver, ActionHelper.class);
	}

	@FindBy(xpath = "//a[@href='/admin/dashboard']//img[@alt='V-Thru']")
	protected WebElement applicationLogo;

	@FindBy(xpath = "//*[@id='exampleEmail']")
	protected WebElement emailAddressTextBox;

	@FindBy(xpath = "//a[@href='/admin/login']")
	protected WebElement signInToExistingAccount;

	@FindBy(xpath = "//button[text()='Recover Password']")
	protected WebElement recoverPasswordButton;

	public boolean insertEmailAddress(String value){
		return objGenericHelper.setElementText(emailAddressTextBox, "email", value);
	}

	public boolean clickSignInToExistingAccountLink(){
		return objGenericHelper.elementClick(signInToExistingAccount, "Sign in to existing account link");
	}

	public boolean clickRecoverPasswordButton(){
		objGenericHelper.elementClick(recoverPasswordButton, "Recover password button");
		if (objGenericHelper.checkInvisbilityOfElement(emailAddressTextBox,"Email Address")){
			return true;
		}else {
			Reporter("Recover button is not clickable", "Fail");
			return false;
		}

	}
}
