/**
 * 
 */
package com.main.uiautomation.configuration.browser;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.main.uiautomation.Utility.ResourceHelper;

public class FirefoxBrowser {
	
	public WebDriver getFirefoxDriver() {
		
		if (System.getProperty("os.name").contains("Mac")){
			WebDriverManager.firefoxdriver().setup();
			return new FirefoxDriver();
		}
		else if(System.getProperty("os.name").contains("Window")){
			WebDriverManager.firefoxdriver().setup();
			return new FirefoxDriver();
		}
		else if(System.getProperty("os.name").contains("Linux")){
			WebDriverManager.firefoxdriver().setup();
			return new FirefoxDriver();
		}
		return null;
	}
}
