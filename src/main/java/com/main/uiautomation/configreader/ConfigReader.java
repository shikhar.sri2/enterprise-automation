/**
 * 
 */
package com.main.uiautomation.configreader;

import com.main.uiautomation.configuration.browser.BrowserType;

public interface ConfigReader {
	public String getApplication();
	public int getPageLoadTimeOut();
	public int getImplicitWait();
	public int getExplicitWait();
	public String getLogLevel();
	public BrowserType getBrowser();
}