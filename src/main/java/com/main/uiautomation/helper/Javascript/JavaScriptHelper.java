/**
 * 
 */
package com.main.uiautomation.helper.Javascript;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.main.uiautomation.helper.TestBase.TestBase;
import com.main.uiautomation.helper.genericHelper.GenericHelper;

public class JavaScriptHelper extends TestBase {
	private WebDriver driver;
	GenericHelper objGenHelper;
	private static final Logger Log = Logger.getLogger(JavaScriptHelper.class);

	public JavaScriptHelper(WebDriver driver) {
		this.driver = driver;
		Log.debug("JavaScriptHelper : " + this.driver.hashCode());
		objGenHelper = PageFactory.initElements(driver, GenericHelper.class);
	}

	public Object executeScript(String script) {
		try {
			JavascriptExecutor exe = (JavascriptExecutor) driver;
			return exe.executeScript(script);
		} catch (Exception e) {
			throw new RuntimeException("Exception while invoking java script" + e.getMessage());
		}
	}

	public void executeScript(String script, Object... args) {
		try {
			JavascriptExecutor exe = (JavascriptExecutor) driver;
			exe.executeScript(script, args);
		} catch (Exception e) {
			throw new RuntimeException("Exception while invoking java script" + e.getMessage());
		}
	}

	/**
	 * This method used to Scroll Window up
	 * 
	 * @param text
	 * @return
	 */
	public boolean scrollUpVertically() {
		try {
			executeScript("window.scrollTo(0, -document.body.scrollHeight);");
			return true;
		} catch (Exception e) {
			Reporter("Exception while scrooling to top of the page", "Fail");
			throw new RuntimeException("Exception while scrooling to top of the page: " + e.getMessage());
		}
	}

	/**
	 * This method used to Scroll Window down
	 * 
	 * @param text
	 * @return
	 */
	public boolean scrollDownVertically(WebDriver driver, String text) {
		try {
			executeScript("window.VerticallyscrollTo(0, document.body.scrollHeight);");
			return true;
		} catch (Exception e) {
			Reporter("Exception while scrooling to the element " + text, "Fail");
			throw new RuntimeException("Exception while scrooling to the element " + text + e.getMessage());
		}
	}

	public boolean scrollToElement(WebDriver driver, WebElement element, String text) {
		try {
			executeScript("window.scrollTo(arguments[0],arguments[1])", element.getLocation().x,
					element.getLocation().y);
			return true;
		} catch (Exception e) {
			Reporter("Exception while scrolling to the element " + text, "Fail");
			throw new RuntimeException("Exception while scrolling to the element " + text + e.getMessage());
		}
	}

	public void scrollToElemetAndClick(WebElement element, String text) {
		scrollToElement(driver, element, text);
		objGenHelper.elementClick(element, text);
	}
}
