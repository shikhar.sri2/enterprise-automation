
package com.main.uiautomation.DataProvider;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.testng.annotations.DataProvider;

import com.main.uiautomation.Utility.ExcelReader;
import com.main.uiautomation.helper.TestBase.TestBase;

public class TestDataProvider {


    private String sTestCaseName;

    @DataProvider(name = "TestRuns")
    public Iterator<Object[]> getEntireSheetData(Method m) {

        sTestCaseName = m.getName();
        ExcelReader excel = new ExcelReader();
        List<Map<String, String>> testRecords = excel.getExcelData(sTestCaseName);
        Collection<Object[]> collection = new ArrayList<Object[]>();

        for (Map<String, String> testRecord : testRecords) {
            collection.add(new Map[]{testRecord});
            TestBase.dataItem.add(testRecord);
        }

        return collection.iterator();
    }

/*	@DataProvider(name = "specifcRow")
    public Iterator<Object[]> getSpecificRowdata() {

        @SuppressWarnings("static-access")
		String sheetName = excel.SheetName;


        List<Map<String, String>> testRecords = excel.getExcelData();
        Collection<Object[]> collection = new ArrayList<Object[]>();

        for (Map<String, String> testRecord : testRecords)
            collection.add(new Map[]{testRecord});

        return collection.iterator();
    }*/
}
