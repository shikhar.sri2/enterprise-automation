package com.enterprise.test.uiautomation.LoginPage;

import com.main.uiautomation.PageObject.ForgotPasswordPage;
import com.main.uiautomation.PageObject.LoginPage;
import com.main.uiautomation.helper.TestBase.TestBase;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class VerifyUserIsAbleToRecoverForgetPassword extends TestBase {


    private static final Logger log = Logger.getLogger(VerifyUserIsAbleToRecoverForgetPassword.class);

    LoginPage loginPage;
    ForgotPasswordPage forgotPasswordPage;

    @BeforeMethod
    public void initializeObjects() {
        loginPage = new LoginPage(driver);
        forgotPasswordPage = new ForgotPasswordPage(driver);
    }

    @Test
    public void VerifyApplicationLogo() throws Exception {
        try {

            Assert.assertTrue(loginPage.forgotPasswordLink(), "Forgot Password link is clicked successfully");
            Assert.assertTrue(forgotPasswordPage.clickRecoverPasswordButton(), "Recover Password button is clicked successfully");


        } catch (Exception e) {
            e.printStackTrace();
            Reporter.log("Test Case Failed", true);
        }
    }
}
