package com.enterprise.test.uiautomation.LoginPage;

import com.main.uiautomation.PageObject.LoginPage;
import com.main.uiautomation.helper.TestBase.TestBase;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class VerifyCompanyLogoIsDisplayedOnScreen extends TestBase {


    private static final Logger log = Logger.getLogger(VerifyCompanyLogoIsDisplayedOnScreen.class);

    LoginPage loginPage;

    @BeforeMethod
    public void initializeObjects() {
        loginPage = new LoginPage(driver);
    }

    @Test
    public void VerifyApplicationLogo() throws Exception {
        try {

            Assert.assertTrue(loginPage.verifyLogoIsDisplayed(), "Application Logo is displayed on screen");

        } catch (Exception e) {
            e.printStackTrace();
            Reporter.log("Test Case Failed", true);
        }
    }
}
